﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace ABPApplication
{
	public class ABPApplicationModule:AbpModule
	{
		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			Configure<AbpAutoMapperOptions>(item =>
			{
				item.AddMaps<ABPApplicationModule>();
			});
		}
	}
}
