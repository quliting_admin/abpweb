﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ABPApplication;
using ABPDomain;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc.Localization;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;
using Volo.Abp.Swashbuckle;

namespace ABPWeb
{
	[DependsOn(typeof(AbpSwashbuckleModule),typeof(AbpAutofacModule),typeof(ABPDomainModule),typeof(ABPApplicationModule))]
	public class ABPWebModule : AbpModule
	{
		public override void OnApplicationInitialization(ApplicationInitializationContext context)
		{
			var app = context.GetApplicationBuilder();
			var env = context.GetEnvironment();

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Error");
			}

			app.UseStaticFiles();
			app.UseRouting();
			app.UseConfiguredEndpoints();
		}

		public override void PreConfigureServices(ServiceConfigurationContext context)
		{
		}

		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			ConfigureSwaggerServices(context.Services);
		}

		private void ConfigureSwaggerServices(IServiceCollection services)
		{
			services.AddSwaggerGen(item =>
			{
				item.SwaggerDoc("v1", new OpenApiInfo() { Title = "ABPWebSwagger", Version = "v1" });
				item.DocInclusionPredicate((_, _) => true);
				item.CustomSchemaIds(item => item.FullName);
			});
		}
	}
}
