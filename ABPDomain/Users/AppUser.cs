﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.Users;

namespace ABPDomain.Users
{
	public class AppUser : FullAuditedAggregateRoot<Guid>, IUser
	{
		public AppUser(Guid id, string name) : base(id) { }
		public Guid? TenantId { get; }
		public string UserName { get; }
		public string Email { get; }
		public string Name { get; }
		public string Surname { get; }
		public bool EmailConfirmed { get; }
		public string PhoneNumber { get; }
		public bool PhoneNumberConfirmed { get; }
		public string Hobby { get; set; }

		private AppUser()
		{

		}
	}
}
