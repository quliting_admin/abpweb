﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace ABPDomain.Data
{
	public class NullMyABPDbSchemaMigrator: IMyABPDbSchemaMigrator, ITransientDependency
	{
		public Task MigrateAsync()
		{
			return Task.CompletedTask;
		}
	}
}
