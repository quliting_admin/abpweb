﻿ 
using Volo.Abp.AuditLogging;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Emailing;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Identity;
using Volo.Abp.IdentityServer;
using Volo.Abp.Modularity;
using Volo.Abp.MultiTenancy;
using Volo.Abp.PermissionManagement.Identity;
using Volo.Abp.PermissionManagement.IdentityServer;
using Volo.Abp.SettingManagement;
using Volo.Abp.TenantManagement;

namespace ABPDomain
{
	//[DependsOn( 
	//	typeof(AbpAuditLoggingDomainModule),
	//	typeof(AbpBackgroundJobsDomainModule),
	//	typeof(AbpFeatureManagementDomainModule),
	//	typeof(AbpIdentityDomainModule),
	//	typeof(AbpPermissionManagementDomainIdentityModule),
	//	typeof(AbpIdentityServerDomainModule),
	//	typeof(AbpPermissionManagementDomainIdentityServerModule),
	//	typeof(AbpSettingManagementDomainModule),
	//	typeof(AbpTenantManagementDomainModule),
	//	typeof(AbpEmailingModule)
	//)]
    public class ABPDomainModule : AbpModule
	{
		public override void ConfigureServices(ServiceConfigurationContext context)
		{ 
			Configure<AbpMultiTenancyOptions>(item =>
			{
				item.IsEnabled = true;
			});
		}
	}
}
